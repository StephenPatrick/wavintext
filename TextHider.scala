import java.io._
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.channels.FileChannel

// This hides some text in an audio file. 
// See the println usage statements for usage guidelines.
object TextHider {
    
    val secretBytes = Array[Byte](115.toByte, 104.toByte, 104.toByte, 104.toByte,
                                  104.toByte, 104.toByte, 104.toByte, 104.toByte,
                                  104.toByte, 104.toByte, 104.toByte, 104.toByte,
                                  104.toByte, 104.toByte, 104.toByte, 104.toByte,
                                  104.toByte, 104.toByte, 104.toByte, 104.toByte,
                                  104.toByte, 104.toByte, 104.toByte, 104.toByte,
                                  104.toByte, 104.toByte, 104.toByte, 104.toByte,
                                  104.toByte, 104.toByte, 104.toByte, 104.toByte)
    val secretHeader = ByteBuffer.wrap(secretBytes)
    val headerSize = 32
    
    def main(args : Array[String]): Unit = {
        
        // Defaults
        var inputAud = "bensound-scifi.wav"
        var inputText = "wonderland.txt"
        var outputAud = "bensound-scifi_copy.wav"
        var outputText = "wonderland_copy.txt"
        
        if (args.length > 1) {
            if (args(0) == "get" && args.length == 3) {
                outputAud = args(1)
                outputText = args(2)
            } else if (args(0) != "get" && args.length == 4) {
                inputAud = args(1)
                inputText = args(2)
                outputAud = args(3)
            } else {
                println("Usage: command line arguments:")
                println("      (1): 'get' OR <anything else> to retrieve / store text")
                println("           uses default inputs: "+ inputAud +","+ inputText +",")
                println("           default outputs (and get input) are concatenated with '_copy'")
                println("      (2): 'get', <path to input aud>, <path to output text>")
                println("      (3): <not get>, <path to input aud>, <path to input text>,")
                println("           <path to output aud>")
                return
            }
        }
        
        try {
            if (args.length > 0) {
                if (args(0) == "get") {
                    getTextFromAud(outputAud, outputText)
                    return
                }
            } 
            storeTextInAud(inputAud, inputText, outputAud)
        } catch {
            case e: FileNotFoundException => {
                println("File not found: " + e.getMessage.split(" ")(0))
            }
        }
    }
    
    def storeTextInAud(inputAud : String, inputText : String, outputAud : String) {
        
        val outCh = new FileOutputStream(outputAud).getChannel()
        val audCh = new FileInputStream(inputAud).getChannel()
        val textCh = new FileInputStream(inputText).getChannel()

        // First, output the wav file
        outCh.transferFrom(audCh,0,audCh.size())
        // Then our header
        outCh.write(secretHeader,audCh.size())
        // Then the text file
        outCh.transferFrom(textCh,audCh.size()+headerSize,textCh.size())
        
        // Right we just stick the text on the end of the wav
        // this works because the wav file format doesn't read
        // past it's defined size and channels. So long as
        // we don't tell it this new data exists, wav
        // players shouldn't try to read the new data.
        // 
        // An alternative would be to put the text in the header,
        // but (it is rumored that) some music players assume
        // a wav's file header is 44 bytes. I didn't test this rumor.
        // 
        // Other audio files work similarly in that they sound the same
        // despite ending with a bunch of junk data.
        //
        // Further alternatives would be to actually obfuscate where 
        // we hide the text inside the wav file. This is 
        // 1. More challenging, and
        // 2. More likely to be detectable by ear--
        //    The most likely method, storing our values in insignificant
        //    bits of music data, could still hypothetically be heard--
        //    especially depending on how wav deals with music data in
        //    the first place.
        //    Another way would be to target pitch ranges above human
        //    hearing. Again, still somewhat likely to be detected, by
        //    dogs or mutants.
        //
        // It would be just as obfuscated and much easier to just
        // encrypt our text with a key, then drop it in the wav file,
        // then decrypt it when we pull it out. We don't do this 
        // currently because demanding a user provide their own
        // ssh key pair dips usability considerably. 

        audCh.close
        textCh.close
        outCh.close
    }

    def getTextFromAud(inputAud : String, outputText : String) {
        
        val audCh = new FileInputStream(inputAud).getChannel()
        
        var secretPosition = findSecretHeader(audCh)

        if (secretPosition != -1) {
            // I don't know why there are these four extra bytes here
            secretPosition = secretPosition + headerSize + 4
            val outCh = new FileOutputStream(outputText).getChannel()
            audCh.transferTo(secretPosition, audCh.size()-secretPosition, outCh)
            outCh.close
        }
        audCh.close
    }
    
    // From the back of the file, retreat until we 
    // find our secret header. If we don't find it, panic
    def findSecretHeader(inputCh : FileChannel): Long = {
        
        val buffer = ByteBuffer.allocate(headerSize)
        var position = inputCh.size()
        while (buffer.compareTo(secretHeader) != 0) {
            if (position < 0) {
                println("Unable to find message in input file.")
                return -1
            }
            inputCh.read(buffer,position)
            buffer.clear()
            position = position - 1
        }
        return position
    }
}