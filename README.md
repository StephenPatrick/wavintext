# README #

scala TextHider.scala  // Hides wonderland.txt inside bensound-scifi.wav 

scala TextHider.scala get // Retrieves wonderland_copy.txt from bensound-scifi_copy.wav

scala TextHider.scala hide inputwav.wav inputtext.text outputwav.wav

scala TextHider.scala get inputwav.wav outputtext.text



Also works maybe:

* inputflac.flac

* inputmp3.mp3

Please don't try to hide multiple messages in the same file. You will lose all messages but the last one hidden and maybe confuse the program into spitting a stack trace at you.